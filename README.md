# CIC Auth Proxy

HOBA challenge/response authentication for cic systems

In PGP configuration...

The user public keys should be hosted at http://localhost:8080/.well-known/publickeys
The detached signature of these keys is hosted at http://localhost:8080/.well-known/signature

The fingerprint of the trusted keys is stored as an env variable called: GPG_TRUSTED_PUBLICKEYS 

Internal endpoints 
# The acl for a given key. Each file under acls corresponds is the key fingerprint and contains acls expressed in yaml

COPY ./meta/acl/test/cic_meta.yml /var/www/acls/d51caf492417f0c2e328a122b3b321d048a89ac6

To run the auth proxy:

```
docker build -t cic-auth-proxy -f docker/Dockerfile . && docker run -p 8080:8080 -p 8081:8081 cic-auth-proxy
```

The env_file in docker/ can be used for sensible defaults when connecting to cic-meta-server in the
standard docker-compose setup:
```
docker run --env-file=docker/env_dev -p 8080:8080 --network cic-network cic-auth-proxy
```


## CONFIGS

### gpg
 homedir = the homedirectory of the gpg keyring
 trusted_publickey_fingerprint = the fingerprint of the trusted key. Comma seperated if multiple.
 publickeys_endpoint = if using the HttpFetcher, the url of the users public keys
 signature_endpoint = if using the HttpFetcher, the url the signature derived from the trusted public key signing the bundle of user publickeys (trusted_publickeys)
 trusted_publickey_filename = if the trusted publickey is not part of the publickey_filename bundle
   then import is seperately with this var, the filename of the trusted publickey 
 gpg_import_dir = the directory path for all FileGetter based pgp imports and validations. e.g. put the
   publickeys and signaure under this dir and refer to them as relative filenames
 publickey_filename = the filename of the publickey bundle relative to gpg_import_dir 
 signature_filename = the filename of the signature relative to gpg_import_dir 
