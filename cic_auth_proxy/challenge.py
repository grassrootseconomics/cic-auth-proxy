# standard imports
import time
import base64


class UWSGIHTTPChallenger:

    def __init__(self, retriever, realm):
        self.retriever = retriever
        self.realm = realm


    def new(self, env):
        (nonce, expires) = self.retriever.challenge(env['REMOTE_ADDR'])
        nonce_b64 = base64.b64encode(nonce)
        expires_duration = expires - time.time()
        wwwauth_header_value  = 'challenge="{}",max-age="{}",realm="{}"'.format(
            nonce_b64.decode('utf-8'),
            int(expires_duration),
            self.realm,
        )
        wwwauth_header = ('WWW-Authenticate', 'HOBA {}'.format(wwwauth_header_value))
        return wwwauth_header
