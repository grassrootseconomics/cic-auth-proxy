from cic_auth.fetcher import Fetcher
from ecuth.challenge import ChallengeRetriever


def noop_resolver(challenge_compare, signature):
    return b'' 


class NoopRetriever(ChallengeRetriever):

    def __init__(self, acl, parser):
        super(NoopRetriever, self).__init__(self.fetch, parser, noop_resolver)
        self.acl = acl
        self.parser = parser


    def fetch(self, auth_digest):
        return self.acl
