# standard imports
import logging
import os
from urllib import request
from urllib.error import HTTPError

# third-party imports
from cic_auth.error import AuthenticationError
from cic_auth.fetcher import Fetcher
from ecuth.ext.pgp import PGPRetriever

logg = logging.getLogger(__name__)


class ProxyFetcher(Fetcher):

    def __init__(self, typ, endpoint):
        self.typ = typ
        self.endpoint = endpoint


    def _decrypt(self, ciphertext):
        return ciphertext


    def load(self, auth_digest):
        auth_digest_hex = auth_digest.hex()
        try:
            resp = request.urlopen(os.path.join(self.endpoint, auth_digest_hex))
            if resp.status != 200:
                raise AuthenticationError(auth_digest)
        except HTTPError as e:
            logg.debug('credentials not found for {} on {}: {}'.format(auth_digest_hex, self.endpoint, e))
            return None

        d = resp.read()
        logg.debug('have credentials {}'.format(d))
        return d


class ProxyChallengeRetriever(PGPRetriever):

    def __init__(self, fetcher, parser, trusted_keys, pgp_dir):
        self.fetcher = fetcher
        super(ProxyChallengeRetriever, self).__init__(fetcher, parser, trusted_keys, pgp_dir)

#
#    def load(self, auth_digest):
#        return self.fetcher.load(auth_digest)
