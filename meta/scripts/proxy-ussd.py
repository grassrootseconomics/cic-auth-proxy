# standard imports
import os
import datetime
import tempfile
import logging
import argparse
import base64
import urllib.request
import re
from urllib.error import URLError, HTTPError 
import hashlib

# external imports
import confini
from http_hoba_auth import hoba_auth_request_string
from http_token_auth import SessionStore
from http_token_auth.error import ExpiredError
        
# local imports
from usumbufu.challenge import Challenger
from usumbufu.filter.sha256 import SHA256Filter
from usumbufu.filter.hoba import HobaFilter
from usumbufu.filter.fetcher import FetcherFilter
from usumbufu.filter.session import SessionFilter
from usumbufu.filter import Filter
from usumbufu.filter.pgp import PGPFilter
from usumbufu.retrieve import Retriever
from usumbufu.retrieve import Fetcher 
from usumbufu.adapters.uwsgi import UWSGIHTTPAuthorization
from usumbufu.adapters.uwsgi import UWSGIAdapter
from usumbufu.adapters.uwsgi import UWSGIQueryString
from usumbufu.retrieve.file import FileFetcher
from usumbufu.error import ChallengeError

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

config_dir = os.environ.get('CONFINI_DIR', os.path.join('/usr/local/etc/cic-auth-proxy'))

argparser = argparse.ArgumentParser()
argparser.add_argument('--trusted-key-file', dest="trusted_key_file", help='A text file containing the trusted public key material')
argparser.add_argument('-c', type=str, default=config_dir, help='config file')
argparser.add_argument('-s', '--shortcircuit-file', dest='s', type=str, help='return file contents for any authentication request')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be more verbose')
args = argparser.parse_args()

if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)

#config_path = os.environ.get('CONFIG_PATH') or  os.path.join(root_dir, 'config/test') 
config = confini.Config(args.c)
config.process()
args_override = {
        'PROXY_SHORTCIRCUIT_FILE': getattr(args, 's'),
        'GPG_TRUSTED_PUBLICKEY_FILENAME': getattr(args, 'trusted_key_file')
        }
config.dict_override(args_override, 'cli flag')
logg.debug('config loaded\n' + str(config))

#gpg_default_dir = os.path.join(root_dir, '.gnupg')
#gpg_dir = config.get('GPG_HOMEDIR', gpg_default_dir)
#gpg = gnupg.GPG(gnupghome=gpg_dir)
gpg_dir = tempfile.mkdtemp()
logg.debug('gpg dir {}'.format(gpg_dir))


#class HttpFilter(Filter):
#  def __init__(self, endpoint, name=None):
#      super(HttpFilter, self).__init__(name=name)
#      self.endpoint = endpoint
#
#  def decode(self, requester_ip, v, signature=None, identity=None):
#        try:
#            resp = request.urlopen(os.path.join(self.endpoint, v))
#            if resp.status != 200:
#                raise AuthenticationError(v)
#        except HTTPError as e:
#            logg.debug('credentials not found for {} on {}: {}'.format(v, self.endpoint, e))
#            return None
#
#        d = resp.read()
#        
#        return (d, signature, identity)


password = config.get('ACL_QUERYSTRING_PASSWORD', '')
username = config.get('ACL_QUERYSTRING_USERNAME', '')

if (not password or not username):
    logg.warn('querystring username or password not set')

user_pass_sha256 = hashlib.sha256(str.encode(username + ':' + password)).hexdigest()

class FetcherAcl(Fetcher):
    def __init__(self, name=None):
        super(FetcherAcl, self).__init__(name=name)
        # echo -n foo:bar | sha256sum
        self.store = { user_pass_sha256: username } 

    def get(self, k):
        return self.store[k.hex()]

    def decode(self, requester_ip, v, signature=None, identity=None):
        logg.debug('in FetcherAcl decode {}'.format(identity))
        return (self.store[identity.decode()], signature, identity)

fetcher_acl = FetcherAcl()
fetcher_filter = FetcherFilter(fetcher_acl)

retriever = Retriever()
retriever.add_decoder(fetcher_filter)

whitelist = config.get('ACL_WHITELIST', '').split(',')

def do_auth(env):
    authenticator = UWSGIAdapter()
    uwsgi_querystring = UWSGIQueryString(retriever, env, whitelist)
    authenticator.register(uwsgi_querystring)
    authenticator.activate(uwsgi_querystring.component_id)

    return authenticator.check()

re_x = r'^HTTP_(X_.+)$'
def add_x_headers(env, header_f):
    for x in env:
        m = re.match(re_x, x)
        if m != None:
            header_orig = m[1].replace('_', '-')
            header_f(header_orig, env[x])

def proxy_pass(env, start_response, headers=[]):
    # do proxy pass
    path = os.path.join(config.get('PROXY_PATH_PREFIX'), env['REQUEST_URI'][1:])
    logg.debug('path {}'.format(config.get('PROXY_PATH_PREFIX')))
    url = '{}://{}:{}{}'.format(
                config.get('PROXY_PROTO'),
                config.get('PROXY_HOST'),
                config.get('PROXY_PORT'),
                path,
                )
    logg.debug('access ok -> {}'.format(url))
    req = urllib.request.Request(url, method=env['REQUEST_METHOD'])
    add_x_headers(env, req.add_header)
    req.add_header('Content-Type', env.get('CONTENT_TYPE', 'application/octet-stream'))
    req.data = env.get('wsgi.input')
    #try:
    res = urllib.request.urlopen(req)


    for h in res.getheaders():
        headers.append(h)

    status = '{} {}'.format(res.status, res.reason)
    content = res.read()
    return (status, content)

# And to conclude, vanilla UWSGI stuff
def application(env, start_response):
    headers = []
    headers.append(('Access-Control-Allow-Origin', '*',))
    headers.append(('Access-Control-Allow-Headers', 'Authorization, Content-Type, X-CIC-Automerge',))
    headers.append(('Access-Control-Expose-Headers', 'WWW-Authenticate, Token',))


    if (env.get('REQUEST_METHOD') == 'OPTIONS'):
        start_response('200 OK', headers)
        return [b'']

    result = None
    try: 
        result = do_auth(env)
    except KeyError:
        logg.error('Auth failed')
        start_response('401 Server Error', headers)
        return [b'']

    if result == None:
        start_response('403 failed miserably', headers)
        return [b'']
# name the successful auth result parts auth_method_component_id = result[0]
    logg.debug('auth result : {}'.format(result))
    auth_method = result[0]
    #auth_string = result[1]
    auth_resource = result[1]
    auth_identity = result[2]
    
    #try:
    #    status, content = proxy_pass(env, start_response, headers) 
    #    start_response(status, headers)
    #    return [content]
    #except HTTPError as e:
    #    logg.warn('The upstream returned an error response {}'.format(e.reason))
    #    start_response('{} {}'.format(e.code, e.reason), headers)
    #    return [b'']
    #except URLError as e:
    #    logg.error('Unable to reach upstream server {}'.format(e.reason))
    #    start_response('502 unable to reach upstream service', headers)
    #    return [b'']


    #session = session_store.get(auth_identity)
    #token = base64.b64encode(session.auth).decode('utf-8')

    #logg.debug('token: {} auth_method: {}'.format(token, auth_method))

    #headers.append(('Token', token))

    start_response('200 OK', headers)
    return [b'']
