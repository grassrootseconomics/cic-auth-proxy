# standard imports
import logging
import os
import re
import base64
import argparse

# external imports
import confini
import gnupg
from cic_auth.adapters.uwsgi import UWSGIHTTPAuthorization
from cic_auth.adapters.uwsgi import UWSGIAdapter
from ecuth.filter.hoba import HobaFilter
from ecuth.filter.pgp import PGPFilter
from yaml_acl.check import YAMLAcl
from yaml_acl.parse import YAMLCredentials
from ecuth.challenge import AuthChallenge, source_hash
import urllib

# local imports
from cic_auth_proxy.challenge import UWSGIHTTPChallenger
from cic_auth_proxy.fetcher import ProxyFetcher
from cic_auth_proxy.fetcher import ProxyChallengeRetriever
from cic_auth_proxy.noop import NoopRetriever

logging.basicConfig(level=logging.WARNING)
logging.getLogger('gnupg').setLevel(logging.DEBUG)
logg = logging.getLogger()

config_dir = os.environ.get('CONFINI_DIR', os.path.join('/usr/local/etc/cic-auth-proxy'))

argparser = argparse.ArgumentParser()
argparser.add_argument('--trusted-key-file', dest="trusted_key_file", help='A text file containing the trusted public key material')
argparser.add_argument('-c', type=str, default=config_dir, help='config file')
argparser.add_argument('-s', '--shortcircuit-file', dest='s', type=str, help='return file contents for any authentication request')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be more verbose')
args = argparser.parse_args()

if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)

#config_path = os.environ.get('CONFIG_PATH') or  os.path.join(root_dir, 'config/test') 
config = confini.Config(args.c)
config.process()
args_override = {
        'PROXY_SHORTCIRCUIT_FILE': getattr(args, 's'),
        'GPG_TRUSTED_PUBLICKEY_MATERIAL': getattr(args, 'trusted_key_file')
        }
config.dict_override(args_override, 'cli flag')
logg.debug('config loaded\n' + str(config))

gpg_default_dir = os.path.join(root_dir, '.gnupg')
gpg_dir = config.get('GPG_HOMEDIR', gpg_default_dir)
gpg = gnupg.GPG(gnupghome=gpg_dir)
logg.debug('gpg dir {}'.format(gpg_dir))

trusted_publickey_material = config.get('GPG_TRUSTED_PUBLICKEY_MATERIAL')
if trusted_publickey_material:
    logg.debug("Loading trusted public key into keystore...")
    f = open(trusted_publickey_material, 'r')
    r = f.read()
    gpg.import_keys(r)
    f.close()

acl_default_path = os.path.join(root_dir, 'acl/test/cic_meta.yml')
acl_path = config.get('ACL_PATH', acl_default_path)
f = open(acl_path, 'r')
r = f.read()
f.close()
acl = YAMLAcl(r)

retriever = None
if config.get('PROXY_SHORTCIRCUIT_FILE'):
    f = open(config.get('PROXY_SHORTCIRCUIT_FILE'), 'r')
    r = f.read()
    f.close()
    retriever = NoopRetriever(r, YAMLCredentials)
else:
    fetcher = ProxyFetcher('meta-proxy', config.get('ACL_CREDENTIALS_ENDPOINT'))
    retriever = ProxyChallengeRetriever(fetcher.load, YAMLCredentials, config.get('GPG_TRUSTED_PUBLICKEYS').split(","), gpg_dir)
    hoba_filter = HobaFilter(config.get('HTTP_AUTH_ORIGIN'), config.get('HTTP_AUTH_REALM'), '969')
    #print(hoba_filter)
    pgp_filter = PGPFilter()
    retriever.add_filter(hoba_filter.filter, 'hoba')
    retriever.add_filter(pgp_filter.filter, 'pgp')

    r = urllib.request.urlopen(config.get('GPG_PUBLICKEYS_ENDPOINT'))
    # r = urllib.request.urlopen(config.get('GPG_TRUSTED_PUBLICKEYS'))
    export = r.read().decode('utf-8')
    r = urllib.request.urlopen(config.get('GPG_SIGNATURE_ENDPOINT'))
    signature = r.read().decode('utf-8')
    retriever.import_keys(export, signature)

hoba_filter = HobaFilter(config.get('HTTP_AUTH_ORIGIN'), config.get('HTTP_AUTH_REALM'), '969')
challenger = UWSGIHTTPChallenger(retriever, config.get('HTTP_AUTH_REALM'))

# TODO: Investigate whether it is possible to process raw headers instead, to avoid extra string massaging
re_x = r'^HTTP_(X_.+)$'
def add_x_headers(env, header_f):
    for x in env:
        m = re.match(re_x, x)
        if m != None:
            header_orig = m[1].replace('_', '-')
            header_f(header_orig, env[x])


def to_action(env):
    if env['REQUEST_METHOD'] == 'GET':
        return 'read'
    else:
        return 'write'

def application(env, start_response):
    headers = []
    content = b''
    status = '401 Unauthorized'

    for k in env.keys():
        logg.debug('uwsgi server var {} {}'.format(k, env[k]))
    #logg.debug('hoba filter: {}'.format(id(hoba_filter)))

    if env.get('REQUEST_METHOD') == 'OPTIONS':
        headers.append(('Access-Control-Allow-Origin', '*',))
        headers.append(('Access-Control-Allow-Headers', 'Authorization, Content-Type, X-CIC-Automerge',))
        start_response('200 OK', headers)
        return [b'']

    # http authorization auth
    authenticator = UWSGIAdapter()
    http_authenticator = UWSGIHTTPAuthorization(retriever, env, 'GE', origin=env.get('HTTP_ORIGIN', 'http://localhost'))
    if hoba_filter != None:
        UWSGIHTTPAuthorization.init_hoba(hoba_filter)
    authenticator.register(http_authenticator)
    authenticator.activate(http_authenticator.component_id)

    headers.append(('Access-Control-Allow-Origin', '*',))
    headers.append(('Connection', 'Keep-Alive',))

    result = authenticator.check()
    if not result:
        if env.get('HTTP_AUTHORIZATION') != None:
            status = '403 You shall not pass'
        else:
            wwwauth_header = challenger.new(env)
            headers.append(wwwauth_header)
        headers.append(('Content-Length', '0',))
        headers.append(('Access-Control-Expose-Headers', 'WWW-Authenticate',))
    else:
        logg.debug('result {}'.format(result))

        if result[0] == 'http-hoba':
            logg.warning('FIX! Sending the wrong token, need the auth token, and method to resolve it to identity')
            token_bytes = result[2].auth
            token_b64 = base64.b64encode(token_bytes)
            headers.append(('Token', token_b64.decode('ascii')),)
            headers.append(('Access-Control-Expose-Headers', 'Token',))

        credentials = result[2] 
        action = to_action(env)
        if acl.check(credentials, env['PATH_INFO'], action):
            path = os.path.join(config.get('PROXY_PATH_PREFIX'), env['REQUEST_URI'][1:])
            logg.debug('path {}'.format(config.get('PROXY_PATH_PREFIX')))
            url = '{}://{}:{}{}'.format(
                        config.get('PROXY_PROTO', 'http'),
                        config.get('PROXY_HOST', 'localhost'),
                        config.get('PROXY_PORT', '8000'),
                        path,
                        )
            logg.debug('access ok -> {}'.format(url))
            req = urllib.request.Request(url, method=env['REQUEST_METHOD'])
            add_x_headers(env, req.add_header)
            req.add_header('Content-Type', env.get('CONTENT_TYPE', 'application/octet-stream'))
            req.data = env.get('wsgi.input')
            res = urllib.request.urlopen(req)

            for h in res.getheaders():
                headers.append(h)

            status = '{} {}'.format(res.status, res.reason)
            content = res.read()

        else:
            status = '403 Hands off'
        headers.append(('Content-Type', 'application/json; charset=UTF-8',)),
        headers.append(('Cache-Control', 'none',)),
        headers.append(('Pragma', 'no-cache',)),


    start_response(status, headers)
    return [content]
