# standard imports
import os
import datetime
import tempfile
import logging
import argparse
import base64
import urllib.request
import re
from urllib.error import URLError, HTTPError 

# external imports
import confini
import http_token_auth
from http_hoba_auth import hoba_auth_request_string
from http_token_auth import SessionStore
# TODO is ExpiredError deprecated?
from http_token_auth.error import ExpiredError
from http_token_auth.error import TokenExpiredError
        
# local imports
from usumbufu.challenge import Challenger
#from usumbufu.challenge import DEFAULT_CHALLENGE_EXPIRE
from usumbufu.filter.sha256 import SHA256Filter
from usumbufu.filter.hoba import HobaFilter
from usumbufu.filter.fetcher import FetcherFilter
from usumbufu.filter.session import SessionFilter
from usumbufu.filter import Filter
from usumbufu.filter.pgp import PGPFilter
from usumbufu.retrieve import Retriever
from usumbufu.retrieve import Fetcher 
from usumbufu.adapters.uwsgi import UWSGIHTTPAuthorization
from usumbufu.adapters.uwsgi import UWSGIAdapter
from usumbufu.retrieve.file import FileFetcher
from usumbufu.error import ChallengeError, AuthenticationError

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

config_dir = os.environ.get('CONFINI_DIR', os.path.join('/usr/local/etc/cic-auth-proxy'))

argparser = argparse.ArgumentParser()
argparser.add_argument('--trusted-key-file', dest="trusted_key_file", help='A text file containing the trusted public key material')
argparser.add_argument('-c', type=str, default=config_dir, help='config file')
argparser.add_argument('-s', '--shortcircuit-file', dest='s', type=str, help='return file contents for any authentication request')
argparser.add_argument('--env-prefix', default=os.environ.get('CONFINI_ENV_PREFIX'), dest='env_prefix', type=str, help='environment prefix for variables to overwrite configuration')
argparser.add_argument('-v', action='store_true', help='be verbose')
argparser.add_argument('-vv', action='store_true', help='be more verbose')
args = argparser.parse_args()

if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)

script_dir = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.dirname(script_dir)

#config_path = os.environ.get('CONFIG_PATH') or  os.path.join(root_dir, 'config/test') 
config = confini.Config(args.c)
config.process()
args_override = {
        'PROXY_SHORTCIRCUIT_FILE': getattr(args, 's'),
        'GPG_TRUSTED_PUBLICKEY_FILENAME': getattr(args, 'trusted_key_file')
        }
config.dict_override(args_override, 'cli flag')
logg.debug('config loaded\n' + str(config))

#gpg_default_dir = os.path.join(root_dir, '.gnupg')
#gpg_dir = config.get('GPG_HOMEDIR', gpg_default_dir)
#gpg = gnupg.GPG(gnupghome=gpg_dir)
gpg_dir = tempfile.mkdtemp()
logg.debug('gpg dir {}'.format(gpg_dir))


#class HttpFilter(Filter):
#  def __init__(self, endpoint, name=None):
#      super(HttpFilter, self).__init__(name=name)
#      self.endpoint = endpoint
#
#  def decode(self, requester_ip, v, signature=None, identity=None):
#        try:
#            resp = request.urlopen(os.path.join(self.endpoint, v))
#            if resp.status != 200:
#                raise AuthenticationError(v)
#        except HTTPError as e:
#            logg.debug('credentials not found for {} on {}: {}'.format(v, self.endpoint, e))
#            return None
#
#        d = resp.read()
#        
#        return (d, signature, identity)


fetcher_pgp_trusted = FileFetcher(config.get('GPG_IMPORT_DIR'))

class FetcherAcl(Fetcher):
    def __init__(self, name=None):
        super(FetcherAcl, self).__init__(name=name)
        self.store = { 'F3FAF668E82EF5124D5187BAEF26F4682343F692': 'foo' } 

    def decode(self, requester_ip, v, signature=None, identity=None):
        return (self.store[identity.hex().upper()], signature, identity)

fetcher_filter = FetcherAcl()
# The Retriever is at the heart of the setup.
# It will run all the decoding steps from all the filters, ultimately resolving to a auth resource (typically ACL) and an identity
challenger = Challenger()

# parse a hoba auth string to key, signature, nonce etc, to the "to be signed" format
hoba_filter = HobaFilter(config.get('HTTP_AUTH_ORIGIN'), config.get('HTTP_AUTH_REALM'), challenger, alg='969')

# hash the "to be signed" format. this yields the exact message the client has signed
hasher_filter = SHA256Filter()

trusted_publickeys = config.get('GPG_TRUSTED_PUBLICKEY_FINGERPRINT', '').split(',') 

pgp_filter = PGPFilter(trusted_publickeys, fetcher_pgp_trusted, gnupg_home=gpg_dir)
pgp_filter.import_keys(config.get('GPG_PUBLICKEY_FILENAME'), config.get('GPG_SIGNATURE_FILENAME'))

# set a session token for the identity, if not yet set
# the session store can be used outside the pipeline to get session info (like the auth token for a Token header response)

default_auth_expire = 60 * 60 * 24 * 7
default_refresh_expire = 60 * 60 * 24
session_store = SessionStore(auth_expire_delta=default_auth_expire, refresh_expire_delta=default_refresh_expire)

session_filter = SessionFilter(session_store)


# the decoders will be run in SEQUENCE. let's wire them up, like so:
hoba_retriever = Retriever()
hoba_retriever.add_decoder(hoba_filter)
hoba_retriever.add_decoder(pgp_filter)
#hoba_retriever.add_decoder(hasher_filter)
hoba_retriever.add_decoder(session_filter)
hoba_retriever.add_decoder(fetcher_filter)


bearer_retriever = Retriever()
bearer_retriever.add_decoder(session_filter)
bearer_retriever.add_decoder(fetcher_filter)

# Below here is the runtime code for the UWSGI application
# Most important to notice here is that the ChallengeRetriever is being passed to the UWSGIHTTPAuthorization object. This object can identify a HOBA request, and will attempt to validate the HOBA auth string using ChallengeRetriever.
# cic_eth.Auth.check() (overloaded) will attempt to FETCH the ACL using the key (if any) resulting from the validation
def do_auth(env):
    authenticator = UWSGIAdapter()
    http_authenticator = UWSGIHTTPAuthorization(hoba_retriever, env, config.get('HTTP_AUTH_REALM'), origin=config.get('HTTP_AUTH_ORIGIN'))
    bearer_authenticator = UWSGIHTTPAuthorization(bearer_retriever, env, config.get('HTTP_AUTH_REALM'), origin=config.get('HTTP_AUTH_ORIGIN'))
    http_authenticator.component_id = 'http-hoba'
    bearer_authenticator.component_id = 'http-bearer'
    
    try:
        authenticator.register(bearer_authenticator)
        authenticator.activate(bearer_authenticator.component_id)
    except TypeError as e:
        logg.debug('not a http bearer request: {}'.format(e))

    try:
        authenticator.register(http_authenticator)
        authenticator.activate(http_authenticator.component_id)
    except TypeError as e:
        logg.debug('not a http hoba request: {}'.format(e))


    return authenticator.check()

re_x = r'^HTTP_(X_.+)$'
def add_x_headers(env, header_f):
    for x in env:
        m = re.match(re_x, x)
        if m != None:
            header_orig = m[1].replace('_', '-')
            header_f(header_orig, env[x])

def proxy_pass(env, start_response, headers=[]):
    # do proxy pass
    path = os.path.join(config.get('PROXY_PATH_PREFIX'), env['REQUEST_URI'][1:])
    logg.debug('path {}'.format(config.get('PROXY_PATH_PREFIX')))
    url = '{}://{}:{}{}'.format(
                config.get('PROXY_PROTO'),
                config.get('PROXY_HOST'),
                config.get('PROXY_PORT'),
                path,
                )
    logg.debug('access ok -> {}'.format(url))
    req = urllib.request.Request(url, method=env['REQUEST_METHOD'])
    add_x_headers(env, req.add_header)
    req.add_header('Content-Type', env.get('CONTENT_TYPE', 'application/octet-stream'))
    req.data = env.get('wsgi.input')
    #try:
    res = urllib.request.urlopen(req)


    #for h in res.getheaders():
    #    headers.append(h)

    status = '{} {}'.format(res.status, res.reason)
    content = res.read()
    return (status, content)

# And to conclude, vanilla UWSGI stuff
def application(env, start_response):
    headers = []
    headers.append(('Access-Control-Allow-Origin', '*',))
    headers.append(('Access-Control-Allow-Headers', 'Authorization, Content-Type, X-CIC-Automerge',))
    headers.append(('Access-Control-Expose-Headers', 'WWW-Authenticate, Token',))


    if (env.get('REQUEST_METHOD') == 'OPTIONS'):
        start_response('200 OK', headers)
        return [b'']

    result = None
    try: 
        result = do_auth(env)
    except ChallengeError as e:
        logg.error('Challenge failed')
        start_response('401 There was an error with the challenge', headers)
        return [b'']
    except TokenExpiredError as e:
        logg.error('Token expired error', e)
        start_response('401 Token expired', headers)
    except AuthenticationError as e:
        logg.error('Failed to authenticate', e)
        start_response('401 Unable to Authenticate', e)
    except KeyError as e:
        logg.error('caught key error, did the Token Expire?', e)
        start_response('401', headers)
    except Exception:
        logg.error(Exception)
        start_response('500 Sorry we messed up...', headers)

    if result == None:
        if env.get('HTTP_AUTHORIZATION') != None:
            start_response('403 failed miserably', headers)
            return [b'']
        (challenge, expire) = challenger.request(env['REMOTE_ADDR'])
        headers.append(('WWW-Authenticate', hoba_auth_request_string(challenge, expire.timestamp(), realm=config.get('HTTP_AUTH_REALM'))),)

        start_response('401 authenticate', headers)
        return [b'']
# name the successful auth result parts auth_method_component_id = result[0]
    auth_method_component_id = result[0]
    auth_string = result[1]
    auth_resource = result[2]
    auth_identity = result[3]
    logg.debug('result {}'.format(result))
    
    if auth_method_component_id == 'http-bearer':
        try:
            status, content = proxy_pass(env, start_response, headers) 
            start_response(status, headers)
            return [content]
        except HTTPError as e:
            logg.warn('The upstream returned an error response {}'.format(e.reason))
            start_response('{} {}'.format(e.code, e.reason), headers)
            return [b'']
        except URLError as e:
            logg.error('Unable to reach upstream server {}'.format(e.reason))
            start_response('502 unable to reach upstream service', headers)
            return [b'']

    logg.debug('Auth success! sring: {} resource: {} identity {}'.format(auth_string, auth_resource, auth_identity))

    session = None
    try:
        session = session_store.get(auth_identity)
    except KeyError as e:
        logg.error('caught key error, did the Token Expire?', e)
        start_response('401', headers)

    if session is not None:
        token = base64.b64encode(session.auth).decode('utf-8')

        logg.debug('token: {} auth_method: {}'.format(token, auth_method_component_id))

        if auth_method_component_id == 'http-hoba':
            headers.append(('Token', base64.b64encode(session.auth).decode('utf-8'),))
        elif session.auth != auth_string:
            headers.append(('Token', base64.b64encode(session.auth).decode('utf-8'),))

        start_response('200 OK', headers)
        return [b'']
    else:
        start_response('500 unexpected error', headers)
        return[b'']
