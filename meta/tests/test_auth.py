# standard imports
import logging
import os
import io
import json

# third-party imports
import pytest

# local imports
from cic_auth_proxy.fetcher import ProxyFetcher
from cic_auth.error import AuthenticationError

logg = logging.getLogger()


class MockResponse:

    def __init__(self, status, content):
        self.status = status
        self.content = content


    def fileno(self):
        return io.BytesIO(self.content.encode('utf-8'))


def test_fetcher_success(
        mocker,
        ):

    o = {
        'level': 9,
        'items': {
            'meta': 4,
            },
        }
    resp = MockResponse(200, json.dumps(o))
    mocker.patch('cic_auth_proxy.fetcher.request.urlopen', return_value=resp)
    f = ProxyFetcher('test-proxy', 'http://any.thing/path')
    digest = os.urandom(32)
    r = f.load(digest)
    logg.debug('r {}'.format(r))


def test_fetcher_fail(
        mocker,
        ):

    resp = MockResponse(404, json.dumps({}))
    mocker.patch('cic_auth_proxy.fetcher.request.urlopen', return_value=resp)
    f = ProxyFetcher('test-proxy', 'http://any.thing/path')
    digest = os.urandom(32)
    with pytest.raises(AuthenticationError):
        f.load(digest)
